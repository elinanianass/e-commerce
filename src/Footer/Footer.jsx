import React from 'react'
import "./Footer.css"
import Fb from "./assets/facebook.png";
import Ig from "./assets/instagram.png";
import In from "./assets/linkedin.png";
import Yt from "./assets/youtube.png";
import Sn from "./assets/send.png";

const Footer = () => {
  return (
    <>
     <section className='bg-dark text-light '>
       <div className='d-flex justify-content-evenly align-items-center'>
        <div>
            <h4 className='text-light'>Website Founders</h4>
              <ul className='list '>
                <li>Oussama EL MABROUKI</li>
                <li>Idriss AGHBALOU</li>
                <li>Anass El INANI</li>
                <li>Anouar SEMMID</li>
              </ul>
        </div>
        <div>
            <h4>Customer Service</h4>
            <ul className='list'>
                <li>Support Team</li>
                <li>Dev Team</li>
                <li>CEO</li>
            </ul>
        </div>
        <div>
            <h4>Contact Info</h4>
            <h6>Here is Contact Info</h6>
            <div className='d-flex justify-content-between'>
            <a href="#"><img src={Fb} alt="Facebook"  width="30px"/></a>
            <a href="#"><img src={Ig} alt="Instagram"width="30px" /></a>
            <a href="#"><img src={In} alt="Linked In" width="30px"/></a>
            <a href="#"><img src={Yt} alt="YouTube"width="30px" /></a>
            </div>
        </div>
        <div>
            <h4>FeedBack</h4>
            <p>We are Happy to give us your Opinion to improve this Website</p>
            <p>Well,Feel free ...</p>
            <form method='post' onClick={e=>e.preventDefault()}>
            <div class="input-group input-group-sm mb-3">
                 <textarea type="text" placeholder='Write ..' class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm"></textarea>
                 <button type='submit'><img src={Sn} width="50px"/></button>
            </div>
            </form>
        </div>
        <div>
            <h4>Other Stuffs</h4>
            <ul className='list'>
            <li>Privacy Policy</li>
            <li>Rules</li>
            </ul>


           </div>
        </div>
        <center>
        <h5>&copy;All Rights Reserved</h5>
        </center>
        
     </section>
    
    </>
  )
}

export default Footer
