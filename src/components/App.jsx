import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Featured from './FeaturedElemnt';


export default  function App() {
    const products = [
        {
            idP: 1,
            nameP: "Product 1",
            pictureP: "https://via.placeholder.com/150",
            categoryP: "Category 1",
            priceP: 100
        },
        {
            idP: 2,
            nameP: "Product 2",
            pictureP: "https://via.placeholder.com/150",
            categoryP: "Category 2",
            priceP: 200
        },
        {
            idP: 3,
            nameP: "Product 3",
            pictureP: "https://via.placeholder.com/150",
            categoryP: "Category 3",
            priceP: 300
        },
        {
            idP: 4,
            nameP: "Product 4",
            pictureP: "https://via.placeholder.com/150",
            categoryP: "Category 4",
            priceP: 400
        },
        {
            idP: 5,
            nameP: "Product 5",
            pictureP: "https://via.placeholder.com/150",
            categoryP: "Category 5",
            priceP: 500
        }
    ]
    return (
        <>  
            {products.map((product) => {
                return (
                    <Featured
                        key={product.idP}
                        idP={product.idP}
                        nameP={product.nameP}
                        pictureP={product.pictureP}
                        categoryP={product.categoryP}
                        priceP={product.priceP}
                    />
                )
            })}
        </>
    )
}