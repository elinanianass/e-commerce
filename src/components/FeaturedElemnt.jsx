import React, { useEffect, useState } from "react";
import Modal from 'react-modal';

export default function FeaturedElement(props) {
    const { idP, nameP, pictureP , categoryP, priceP} = props
    const [showModal, setShowModal] = useState(false)
    const [quantity, setQuantity] = useState(1)
    const [total, setTotal] = useState(quantity * priceP);
    
    //Open Modal
    const openModal = () => {
        setShowModal(true)
    }
    //Close Modal
    const closeModal = () => {
        setShowModal(false)
    }
    //Change Quantity value
    const changeQuantity = ({target: {value}}) => {
        setQuantity(value)
    }

    //Calculate Total when quantity change
    useEffect(() => {
        setTotal(quantity * priceP)
    }, [quantity])

    return (
      <>
        <div className="card" >
          <img
            src={pictureP}
            alt={nameP}
            className="card-img-top"
            loading="lazy"
            sizes="150px"
          />
          <div className="card-body">
            <h4 className="card-title">{nameP}</h4>
            <h5 className="card-text">{categoryP}</h5>
            <h6 className="card-text">{priceP}</h6>
            <button className="btn btn-dark text-light" onClick={openModal}>
              Buy
            </button>
          </div>
        </div>
        <Modal isOpen={showModal}>
          <div className="container">
            <div className="row">
              <img
                src={pictureP}
                alt={nameP}
                loading="lazy"
                className="span-3"
              />
              <div className="span-9 container">
                <form onSubmit={e=>e.preventDefault()}>
                  <div className="form-group">
                    <label htmlFor="name">Name</label>
                    <input
                      type="text"
                      className="form-control"
                      id="name"
                      value={nameP}
                      disabled={true}
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="category">Category</label>
                    <input
                      type="text"
                      className="form-control"
                      id="category"
                      value={categoryP}
                      disabled={true}
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="price">Price</label>
                    <input
                      type="text"
                      className="form-control"
                      id="price"
                      value={priceP}
                      disabled={true}
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="quantity">Quantity</label>
                    <input
                      type="number"
                      className="form-control"
                      id="quantity"
                      value={quantity}
                      onChange={changeQuantity}
                    />
                    <div className="form-group">
                        <label htmlFor="total">Total</label>
                        <input
                            type="text"
                            className="form-control"
                            id="total"
                            value={total}
                            disabled={true}
                        />
                    </div>
                    <div className="form-group">
                        <button className="btn btn-success">Buy</button>
                        <button className="btn btn-danger" onClick={closeModal}>Cancel</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </Modal>
      </>
    );
}